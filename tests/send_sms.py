#!/usr/bin/python3
from gi.repository import GLib
import dbus
import dbus.mainloop.glib
import sys

def get_active_modem():
  """Goal: Find and return the first active modem DBUS path.
  IE: /org/freedesktop/ModemManager1/Modem/0

  I've noticed the modem will change if the phone goes to sleep so this
  will need to be called before interacting with the modem each time.
  There's probably a better way to do this but I cant figure it out.

  Success - return String of modem
  Fail    - return None
  """ #TODO - I bet there's a better way to do this.

  modem_objects = None
  active_modem = None

  #build the dbus connection to search for modem
  system_bus = dbus.SystemBus()
  mm_modem = system_bus.get_object('org.freedesktop.ModemManager1', '/org/freedesktop/ModemManager1')
  bus_iface = dbus.Interface(mm_modem, dbus_interface='org.freedesktop.DBus.ObjectManager')

  #Try and actually Search for the modem path on the dbus.
  try:
    modem_objects = bus_iface.GetManagedObjects('/org/freedesktop/ModemManager1/Modem')

  except Exception as problem:
    print(f"Problem looking for modem: {problem} ")


  #pull out the dbus path of the modem, If we have one.
  if(modem_objects != None):
    for modem in modem_objects:
      active_modem = modem
      break

  return active_modem

def sms_create(active_modem, number, text, delivery_report_request=True):
  """Goal: Create an SMS message on the Modem. This is required before
  we can do anything with it. 

  Success - return string of dbus path of new message on modem.
  Fail    - return None
  """
  dbus_sms_url = None


  #Build up the DBUS messaging interface to create a new SMS
  system_bus = dbus.SystemBus()
  dbus_modem_object = system_bus.get_object('org.freedesktop.ModemManager1', active_modem)
  dbus_messaging_interface = dbus.Interface(dbus_modem_object, dbus_interface='org.freedesktop.ModemManager1.Modem.Messaging')

  #build the data pack to send across dbus to MM
  dbus_sms_package = dbus.Dictionary( {"number" : number, "text" : text, "delivery-report-request": delivery_report_request }, signature='sv')

  #Send the data pack to MM and get the ID.
  try:
    dbus_sms_url = dbus_messaging_interface.Create(dbus_sms_package)
  except Exception as problem:
    print(f"Problem Creating message on Modem. Are you sure it's active?")

  #return the created SMS dbus path.
  return dbus_sms_url

def sms_send(dbus_sms_url):
  """Goal: Take a dbus path of a created SMS message and attempt to send
  it via Modem Manager.

  Success - return 0
  Fail    - return None
  """
  send_sms_return = None
  
  #Now that we have the new sms created and its id attach to it
  system_bus = dbus.SystemBus()
  dbus_sms_object = system_bus.get_object('org.freedesktop.ModemManager1', dbus_sms_url)
  dbus_sms_interface = dbus.Interface(dbus_sms_object, dbus_interface='org.freedesktop.ModemManager1.Sms')

  try:
    #the MM to send the new sms we created.
    send_sms_return = dbus_sms_interface.Send()
    #print(f"Is this always None? -{send_sms_return}-")
    send_sms_return = 0

  except Exception as problem:
    print(f"Problem sending SMS: {problem} ")

  return send_sms_return

def delete_sms(active_modem, dbus_sms_url):
  """Goal: Delete an SMS message off the Modem.

  Success - return 0
  Fail    - return None
  """
  delete_sms_return = None

  #Build up the messaging interface to delete an sms
  system_bus = dbus.SystemBus()
  dbus_modem_object = system_bus.get_object('org.freedesktop.ModemManager1', active_modem)
  dbus_messaging_interface = dbus.Interface(dbus_modem_object, dbus_interface='org.freedesktop.ModemManager1.Modem.Messaging')

  try:
    #using the path delete the sms.
    delete_sms_return = dbus_messaging_interface.Delete(dbus_sms_url)
    #print(f"is this always None for delete too? -{delete_sms_return}-")
    delete_sms_return = 0

  except Exception as problem:
    print(f"Cant delete SMS: {problem} ")


  return delete_sms_return


#start main

if(len(sys.argv) != 3):
  print(f"usage: {sys.argv[0]} 1235550987 \"Hello World!\" " )
  sys.exit(1)

else:
  number = str(sys.argv[1])
  message = str(sys.argv[2])
  print(f"Sending SMS to: {number} with the text: {message}")


mm_active_modem_dbus_path = get_active_modem()
if( mm_active_modem_dbus_path != None ):
  print(f"Active Modem: {mm_active_modem_dbus_path}")

  #trying to create a new SMS message on the modem.
  new_sms_on_mm = sms_create(mm_active_modem_dbus_path, number, message)

  #if successful, Try to send it.
  if( new_sms_on_mm != None ):
    print(f"SMS Message: {new_sms_on_mm}")
    sent_status = sms_send(new_sms_on_mm)

    #if it was sent, We can remove it from the modem.
    if(sent_status != None and sent_status == 0):
      print(f"SMS sent")
      delete_status = delete_sms(mm_active_modem_dbus_path, new_sms_on_mm)

      #Print status of delete.
      if(delete_status != None and sent_status == 0):
        print(f"SMS deleted from Modem.")
      else:
        print(f"Failed to Delete SMS.")


    else:
      print(f"Failed to Send SMS message.")

  else:
    print(f"Failed to create SMS on modem.")


else:
  print(f"Cant find any Modems.")



#mmcli -m 0 -s /org/freedesktop/ModemManager1/SMS/XX
""" SMS on Modem after create: 
  -----------------------------
  General    |       dbus path: /org/freedesktop/ModemManager1/SMS/13
  -----------------------------
  Content    |          number: 1235550987
             |            text: test
  -----------------------------
  Properties |        pdu type: submit
             | delivery report: requested
"""

"""SMS on Modem After create - Modem disabled then reenabled - Broken, weird state, cant send or delete.
  ----------------------------
  General    |      dbus path: /org/freedesktop/ModemManager1/SMS/18
  ----------------------------
  Properties |          class: 0
             | delivery state: completed-received

"""

""" SMS on Modem after send - Successful with delivery report
  -------------------------------
  General    |         dbus path: /org/freedesktop/ModemManager1/SMS/15
  -------------------------------
  Content    |            number: 1235550987
             |              text: test
  -------------------------------
  Properties |          pdu type: submit
             |             state: sent
             |   delivery report: requested
             | message reference: 150

"""

""" SMS on Modem after send - Successful without delivery report (False or left out entirely)
  -------------------------------
  General    |         dbus path: /org/freedesktop/ModemManager1/SMS/16
  -------------------------------
  Content    |            number: 1235550987
             |              text: test
  -------------------------------
  Properties |          pdu type: submit
             |             state: sent
             |   delivery report: not requested
             | message reference: 151
"""

""" SMS on Modem after send - failed with delivery report
"""

""" SMS on Modem after send - failed without delivery report
"""



