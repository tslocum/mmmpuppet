#!/usr/bin/python3

from pathlib import Path
from gi.repository import GLib
import dbus
import dbus.mainloop.glib

def message_added(dbus_mms_path, mms_data):
  print(f"hi")

  if( mms_data['Status'] != "received" ):
    print(f"Sending an MMS file")
    return 0

  #build a new list of recipients to get rid of the dbus.whatever
  # and so we can .remove our own number.
  mms_data_recipients = []
  for number in mms_data['Recipients']:
    mms_data_recipients.append(str(number))

  print(f"MMS recipients {str(mms_data_recipients)}")
  print(f"MMS Sender {str(mms_data['Sender'])}.")



  for attachment in mms_data['Attachments']:

    in_file = open(attachment[2], "rb")
    header  = in_file.read(attachment[3]) #Smil
    header  = None #get rid of it
    mms_data    = in_file.read(attachment[4])
    in_file.close()

    #if the data is just text (typically a group message) just get it out.
    if( attachment[1] == "text/plain;charset=utf-8" ):
      print(f"Text in our MMS: {mms_data.decode('utf-8')} ")

    else:
      attachment_file_to_bridge = "/tmp/"+attachment[2].split("/",6)[5]+"-"+str(attachment[4])+"."+attachment[1].split("/",2)[1]
      opened_attachment_file = open(attachment_file_to_bridge, "wb")
      opened_attachment_file.write(mms_data)
      opened_attachment_file.close()
      print(f"File: {attachment_file_to_bridge}")


  return 0



dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
mainloop=GLib.MainLoop()


#Start watching for new messages from mmsd
session_bus = dbus.SessionBus()
session_bus.add_signal_receiver( handler_function = message_added,
                                 bus_name="org.ofono.mms",
                                 path = "/org/ofono/mms/modemmanager",
                                 dbus_interface = "org.ofono.mms.Service",
                                 signal_name = "MessageAdded")

mainloop.run()
