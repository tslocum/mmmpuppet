#!/usr/bin/python3

import mmm_common
import mmm_log


#Database
import sqlite3

#For random mms group name in db
import time
import random

def initialize():
  mmm_log.info("mmm_database.initialize", "Called.")

  try:
    database_connection = sqlite3.connect(mmm_common.DATABASE_FILE)
    
  except Exception as problem:
    mmm_log.critical("mmm_database.initialize", problem)
    return None

  cursor = database_connection.cursor()
  cursor.execute('''CREATE TABLE IF NOT EXISTS messages (
                      id                      INTEGER PRIMARY KEY, 
                      rooms_id                INTEGER, 
                      state                   INTEGER,
                      number                  TEXT,
                      mmsgroup                TEXT,
                      text                    TEXT,
                      data                    TEXT,
                      service_category        INTEGER,
                      delivery_report_request INTEGER,
                      timestamp               TEXT,
                      delivery_state          INTEGER,
                      content_type            TEXT,
                      size                    INTEGER,
                      bridged                 INTEGER,
                      bridged_attempt         INTEGER,
                      event                   TEXT )''')
  database_connection.commit()


  cursor.execute('''CREATE TABLE IF NOT EXISTS rooms (
                      id     INTEGER PRIMARY KEY, 
                      room   TEXT,
                      member TEXT )''')
  database_connection.commit()

 
  cursor.execute('''CREATE TABLE IF NOT EXISTS group_members (
                      id       INTEGER PRIMARY KEY, 
                      mmsgroup TEXT,
                      number   TEXT )''')
  database_connection.commit()


  cursor.execute('''CREATE TABLE IF NOT EXISTS matrix_sync (
                      id    INTEGER PRIMARY KEY, 
                      since_token TEXT )''')
  database_connection.commit()


  #check for ID 1 in matrix_sync. if nothing returns enter something.
  cursor.execute('''SELECT id FROM matrix_sync WHERE id=?''', (1,))
  database_connection.commit()

  
  #Try and get row id 1.
  row = cursor.fetchone()

  #If there is no row in the DB, create one.
  if( row == None ):
    cursor.execute('''INSERT INTO matrix_sync (since_token) VALUES( ? );''',  ("fresh_start",) )
    database_connection.commit()


  database_connection.close

  return 0

def write_sync_callback(since_token):
  mmm_log.info("mmm_database.write_sync_callback", "Called.")

  try:
    database_connection = sqlite3.connect(mmm_common.DATABASE_FILE)
    
  except Exception as problem:
    mmm_log.critical("mmm_database.write_sync_callback", problem)
    return None

  cursor = database_connection.cursor()
  cursor.execute('''update matrix_sync set since_token=? where id=1''',  (since_token,) )
  database_connection.commit()
  database_connection.close
  mmm_log.info("mmm_database.write_sync_callback", "Done.")

  return 1

def get_token():
  mmm_log.info("mmm_database.get_token", "Called.")

  try:
    database_connection = sqlite3.connect(mmm_common.DATABASE_FILE)
    
  except Exception as problem:
    mmm_log.critical("mmm_database.get_token", problem)
    return None

  cursor = database_connection.cursor()
  
  #check for ID 1 in matrix_sync. if nothing returns enter something.
  cursor.execute('''SELECT since_token FROM matrix_sync WHERE id=?''', (1,))
  database_connection.commit()

  #get the first row and return it
  row = cursor.fetchone()

  database_connection.close

  if( row == None ):
    mmm_log.info("mmm_database.get_token", "No token.")
    return None
  else:
    mmm_log.info("mmm_database.get_token", "found since_token.")
    mmm_log.debug("mmm_database.get_token", "since_token: " + row[0])
    return row[0]

def write_new_message(message):
  """Goal: Take a message pack and write it to the Database.
  return None - Database failure
  return 0    - Didn't insert
  return > 0  - ID of inserted message
  """
  mmm_log.info("mmm_database.write_new_message", "Called.")

  message_id = 0

  try:
    database_connection = sqlite3.connect(mmm_common.DATABASE_FILE)

  except Exception as problem:
    mmm_log.info("mmm_database.write_new_message", "Problem connecting to the database")
    mmm_log.debug("mmm_database.write_new_message", "Exception " + str(problem) )
    return None

  cursor = database_connection.cursor()

  try:
    cursor.execute('''INSERT INTO messages(rooms_id, state, number, mmsgroup, text, data, service_category, delivery_report_request, timestamp, delivery_state, content_type, size, bridged, bridged_attempt, event)
                 VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)''', 
                 (message["rooms_id"], message["state"], message["number"], message["mmsgroup"], message["text"], message["data"], message["service_category"], message["delivery_report_request"], message["timestamp"], message["delivery_state"], message["content_type"], message["size"], message["bridged"], message["bridged_attempt"], message["event"], ))

  except Exception as problem:
    mmm_log.info("mmm_database.write_new_message", "with insert statment")
    mmm_log.debug("mmm_database.write_new_message", "Exception " + str(problem) )
    message_id = 0

  database_connection.commit()

  if( cursor.lastrowid != None ):
    message_id = cursor.lastrowid

  database_connection.close

  return message_id

def find_mms_group(mms_group_message_number_list):
  """Goal: take all of the Phone Numbers (2 or more) and return the
  group:12345678 information.

  return None                 - problem with Database
  return 0                    - no Group 
  return.startswith("group:") - it's a group
  """#Note to self = Good
  mmm_log.info("mmm_database.find_mms_group", "Called.")

  mms_group_name = 0


  #try and remove your number. we dont need it.
  try:
    mms_group_message_number_list.remove(mmm_common.my_formatted_cell_number)
  except Exception as problem:
    mmm_log.info("mmm_database.find_mms_group", "Your number not in the list. No big deal, We dont want it there anyway.")

  #sort the list so we can compare later.
  mms_group_message_number_list.sort()



  try:
    database_connection = sqlite3.connect(mmm_common.DATABASE_FILE)
    cursor = database_connection.cursor()

  except Exception as problem:
    mmm_log.critical("mmm_database.find_mms_group", problem)
    return None



  #Get all of the groups the sender is in.
  cursor.execute('''SELECT mmsgroup FROM group_members WHERE number=?''', (mms_group_message_number_list[0],))
  database_connection.commit()
  all_groups_sender_is_in = cursor.fetchall()

  #go though all the group the sender is in.
  for group in all_groups_sender_is_in:
    
    cursor.execute('''SELECT number FROM group_members WHERE mmsgroup=?''', (group[0],))
    database_connection.commit()
    all_numbers_in_group = cursor.fetchall()

    #Build a list of all the numbers in that group
    database_list_of_numbers = []
    for number in all_numbers_in_group:
      database_list_of_numbers.append(number[0])

    #sort the list and compare it with the mms group message number list
    database_list_of_numbers.sort()

    
    if( database_list_of_numbers ==  mms_group_message_number_list ):
      mms_group_name = group[0]
      break


  database_connection.close

  return mms_group_name

def create_mms_group(everyone_list):
  """Goal: create a new MMS group in the Database. We're not creating a
  room or checking if we already have a group, Just make one. This work
  should be done outside of this.

  Return None                - Problem with Database
  return.startswith("group") - its a group
  """ #Note to self = good
  mmm_log.info("mmm_database.create_mms_group", "Called.")

  group = "group:"+str(int(time.time()))+str(random.randrange(10, 99))

  try:
    everyone_list.remove(mmm_common.my_formatted_cell_number)
  except Exception as problem:
    mmm_log.info("mmm_database.create_mms_group", "Your number not in the list. We don't want it there anyway ")


  try:
    database_connection = sqlite3.connect(mmm_common.DATABASE_FILE)
    
  except Exception as problem:
    mmm_log.critical("mmm_database.create_mms_group", "Problem with Database.")
    mmm_log.debug("mmm_database.create_mms_group", "Exception " + str(problem))
    return None

  cursor = database_connection.cursor()

  for number in everyone_list:
    cursor.execute('''INSERT INTO group_members (mmsgroup,number) VALUES(?,?)''', (group,number,))
    database_connection.commit()

  row_id = cursor.lastrowid
  database_connection.close
  
  if( row_id == None ):
    mmm_log.error("mmm_database.create_mms_group", "Something weird happened. Failed to write to database?")
    return None
  elif( row_id > 0 ):
    mmm_log.info("mmm_database.create_mms_group", "Written to Database")
    return group
  else:
    mmm_log.error("mmm_database.create_mms_group", "Something weird happened?")
    return None

def find_matrix_room_id(number="", group=""):
  """Goal: Take the cell number or Group ID and return the matrix room.

  return None - Problem with Database
  return 0    - Good, Local room No matrix ID. This is fine.
  return 1    - Not even a local room.
  return.startswith("!") - we have a room.
  """#Note to self = good.
  mmm_log.info("mmm_database.find_matrix_room_id", "Called.")

  matrix_room_id = 0

  try:
    database_connection = sqlite3.connect(mmm_common.DATABASE_FILE)

  except Exception as problem:
    mmm_log.critical("mmm_database.find_matrix_room_id", "Problem with database!")
    mmm_log.debug("mmm_database.find_matrix_room_id", "Exception " + str(problem) )
    return None

  cursor = database_connection.cursor()

  if( group == "" ):
    cursor.execute('''SELECT room, member FROM rooms WHERE member=?''', (number,))

  else:
    cursor.execute('''SELECT room, member FROM rooms WHERE member=?''', (group,))


  database_connection.commit()
  row = cursor.fetchone()
  database_connection.close

  if( row == None ):
    mmm_log.warning("mmm_database.find_matrix_room_id", "We dont even have a local room. this is bad.")
    matrix_room_id = 1
  elif( row[0] == "" ):
    mmm_log.info("mmm_database.find_matrix_room_id", "local room but no matrix room. You'll have to make one.")
    matrix_room_id = 0
  elif( row[0].startswith("!") ):
    mmm_log.info("mmm_database.find_matrix_room_id", "Found a room")
    matrix_room_id = row[0]
  else:
    mmm_log.critical("mmm_database.find_matrix_room_id", "Something unexpected happened.")
    return None

  return matrix_room_id

def delete_room(matrix_id):
  """This cleans up the database when a user leaves the room.
  return None - Fail
  return row id we deleted - success 
  """
  mmm_log.info("mmm_database.delete_room", "Called.")

  try:
    database_connection = sqlite3.connect(mmm_common.DATABASE_FILE)
    
  except Exception as problem:
    mmm_log.critical("mmm_database.delete_room", problem)
    return None

  cursor = database_connection.cursor()

  cursor.execute('''SELECT id, member FROM rooms WHERE room=?''', (matrix_id,))
  database_connection.commit()
  row = cursor.fetchone()

  room_id = 0

  if( row != None ):
    if( row[1].startswith("group")):
      mmm_log.info("mmm_database.delete_room", "Found a group chat.")
      cursor.execute('''DELETE FROM group_members WHERE mmsgroup=?''', (row[1],))
      database_connection.commit()
      cursor.execute('''DELETE FROM rooms WHERE room=?''', (matrix_id,))
      database_connection.commit()
      room_id = row[0]

    else:
      mmm_log.info("mmm_database.delete_room", "Not a group chat.")
      cursor.execute('''DELETE FROM rooms WHERE room=?''', (matrix_id,))
      database_connection.commit()
      room_id = row[0]
   
 
  database_connection.close

  
  return room_id

def delete_room_messages(rooms_id):
  """Take the room ID and delete all messages in that room
  return none on fail
  return last row we touch on success.
  """
  mmm_log.info("mmm_database.delete_room_messages", "Called.")

  try:
    database_connection = sqlite3.connect(mmm_common.DATABASE_FILE)
    
  except Exception as problem:
    mmm_log.critical("mmm_database.delete_room_messages", problem)
    return None

  cursor = database_connection.cursor()

  cursor.execute('''DELETE FROM messages WHERE rooms_id=?''', (rooms_id,))
  database_connection.commit()


  row_id = cursor.lastrowid
  database_connection.close

  return row_id

def find_local_room_id(member="", matrix_room_id=""):
  """find the room_ID for a new message.

  return None on failure
  return 0 to say no room.
  return room id (id > 1) if success.
  """#Note for myself = Good
  mmm_log.info("mmm_database.find_local_room_id", "Called.")

  room_id = 0

  if( member == "" and  matrix_room_id == "" ):
    mmm_log.error("mmm_database.find_local_room_id", "No information given")
    return None

  try:
    database_connection = sqlite3.connect(mmm_common.DATABASE_FILE)
  except Exception as problem:
    mmm_log.critical("mmm_database.find_local_room_id", problem)
    return None

  cursor = database_connection.cursor()
  
  if( matrix_room_id != "" ):
    cursor.execute('''SELECT id FROM rooms WHERE room=?''', (matrix_room_id,))

  else:
    cursor.execute('''SELECT id FROM rooms WHERE member=?''', (member,))



  database_connection.commit()
  row = cursor.fetchone()
  database_connection.close

  if( row != None and row[0] > 0 ):
    room_id = row[0]

  return room_id

def create_local_room_id(member="", matrix_room_id=""):
  """create a local room for a new message. We dont need a matrix room
  at this point. Just worried about getting the message in the database.

  Return None - Failure
  return > 1 - the ID in the database, success.
  """
  mmm_log.info("mmm_database.create_local_room", "Called.")

  try:
    database_connection = sqlite3.connect(mmm_common.DATABASE_FILE)
  except Exception as problem:
    mmm_log.critical("mmm_database.create_local_room", problem)
    return None

  cursor = database_connection.cursor()


  cursor.execute('''INSERT INTO rooms(room, member) VALUES(?,?)''',  (matrix_room_id, member,) )


  database_connection.commit()
  row_id = cursor.lastrowid

  database_connection.close
  return row_id

def add_matirx_room_to_local_room(rooms_table_id, matrix_room_id):
  """Take a rooms database table ID and update it with a matirx room.

  return rowID on success
  return None on fail
  """
  mmm_log.info("mmm_database.add_matirx_room_to_local_room", "Called.")

  try:
    database_connection = sqlite3.connect(mmm_common.DATABASE_FILE)
    
  except Exception as problem:
    mmm_log.critical("mmm_database.add_matirx_room_to_local_room", problem)
    return None

  cursor = database_connection.cursor()
  cursor.execute('''UPDATE rooms SET room=? WHERE id=?''',  (matrix_room_id, rooms_table_id,) )
  database_connection.commit()
  database_connection.close
  row_id = cursor.lastrowid
  
  return row_id

def matrix_room_to_numbers(matrix_room_id):
  """Goal: take matrix room ID and return a list of numbers. If this is
  a group, it will have more than one.

  return None - Problem with database
  return 0    - no room
  return 1    - 1:1 text
  return 2+   - group chat.
  """#Note to self = good
  mmm_log.info("mmm_database.matrix_room_to_numbers", "Called.")

  number_list = []

  try:
    database_connection = sqlite3.connect(mmm_common.DATABASE_FILE)
    
  except Exception as problem:
    mmm_log.info("mmm_database.matrix_room_to_numbers", "problem with database. ")
    mmm_log.critical("mmm_database.matrix_room_to_numbers", "Exception " + str(problem) )
    return None

  cursor = database_connection.cursor()
  
  cursor.execute('''SELECT member FROM rooms WHERE room=?''', (matrix_room_id,))
  database_connection.commit()
  rooms_row = cursor.fetchone()

  if( rooms_row ):

    if( rooms_row[0].startswith("group") ):
      #check the group_memebers of that group
      cursor.execute('''SELECT number FROM group_members WHERE mmsgroup=?''', (rooms_row[0],))
      database_connection.commit()

      #Write all the members(numbers) into a list
      all_members = cursor.fetchall()
      for number in all_members:
        number_list.append(number[0])


    else:
      number_list.append(rooms_row[0])



  database_connection.close
  return number_list

def mark_message_as_bridged(message_id):
  """Goal, Mark Message ID as bridged in database and return updated id.

  return None - failed
  return > 0  - Success
  """#note to self = good
  mmm_log.info("mmm_database.mark_message_as_bridged", "Called.")

  row_id = None

  try:
    database_connection = sqlite3.connect(mmm_common.DATABASE_FILE)
    
  except Exception as problem:
    mmm_log.critical("mmm_database.mark_message_as_bridged", "Cant grab Database")
    mmm_log.debug("mmm_database.mark_message_as_bridged", "Exception " + str(problem))
    return None

  cursor = database_connection.cursor()
  cursor.execute('''UPDATE messages SET bridged=? where id=?''',  (True, message_id,) )
  database_connection.commit()


  cursor.execute('''SELECT id FROM messages WHERE bridged=? and id=?''', (True, message_id,))
  database_connection.commit()
  
  row = cursor.fetchone()
  database_connection.close


  if( row != None and row[0] > 0 ):
    row_id = row[0]
    mmm_log.info("mmm_database.mark_message_as_bridged", "Marked as bridged!")
    mmm_log.debug("mmm_database.mark_message_as_bridged", "ID: " + str(row_id) +" Marked as bridged" )

  else:
    mmm_log.info("mmm_database.mark_message_as_bridged", "Cant find message to mark as bridged.")
    mmm_log.debug("mmm_database.mark_message_as_bridged", "Problem with message database ID: " + str(message_id) )


  return row_id

def local_group_to_number_list(local_group_name):
  """Goal, Take a group (start with "group:") and return a list of
  numbers.
  return None      - cant access database
  return list of 0 - failed
  return list of 1 - something really, weird, this isn't a group.
  return list of 2 - success.
  """#Note to Self - Good
  mmm_log.info("mmm_database.matrix_room_to_numbers", "Called.")

  try:
    database_connection = sqlite3.connect(mmm_common.DATABASE_FILE)
    
  except Exception as problem:
    mmm_log.critical("mmm_database.get_token", problem)
    return None

  cursor = database_connection.cursor()

  number_list = []

  cursor.execute('''SELECT number FROM group_members WHERE mmsgroup=?''', (local_group_name,))
  database_connection.commit()

  #Write all the members(numbers) into a list
  all_members = cursor.fetchall()
  for number in all_members:
    number_list.append(number[0])



  database_connection.close
  return number_list

  return None
